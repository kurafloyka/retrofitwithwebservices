package com.example.retrofitwithwebservices.practise4;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.example.retrofitwithwebservices.R;
import com.example.retrofitwithwebservices.practise1.Models.Comment;
import com.example.retrofitwithwebservices.practise1.Models.CommentResult;
import com.example.retrofitwithwebservices.practise1.RestApi.ManagerAll;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Main5Activity extends AppCompatActivity {
    String id, postId;
    List<CommentResult> listCommentResult;
    TextView idText, postIdText, nameText, emailText, bodyText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main5);

        getPostId();
        request();
        define();


    }


    public void setLayout(List<CommentResult> list) {

        idText.setText(list.get(0).getId().toString());
        postIdText.setText(list.get(0).getPostId().toString());
        nameText.setText(list.get(0).getName());
        emailText.setText(list.get(0).getEmail());
        bodyText.setText(list.get(0).getBody());
    }

    public void define() {
        idText = findViewById(R.id.id);
        postIdText = findViewById(R.id.postId);
        nameText = findViewById(R.id.name);
        emailText = findViewById(R.id.email);
        bodyText = findViewById(R.id.body);
    }


    public void getPostId() {

        Bundle bundle = getIntent().getExtras();
        id = bundle.getString("id");
        postId = bundle.getString("postId");

        Log.i("ID : ", "id : " + id + "postId : " + postId);

    }

    public void request() {
        listCommentResult = new ArrayList<>();

        Call<List<CommentResult>> commentResultList = ManagerAll.getInstance().commentResultCall(postId, id);

        commentResultList.enqueue(new Callback<List<CommentResult>>() {
            @Override
            public void onResponse(Call<List<CommentResult>> call, Response<List<CommentResult>> response) {

                if (response.isSuccessful()) {
                    Log.i("Response : ", response.body().toString());
                    listCommentResult = response.body();
                    setLayout(listCommentResult);
                }
            }

            @Override
            public void onFailure(Call<List<CommentResult>> call, Throwable t) {

            }
        });
    }


}
