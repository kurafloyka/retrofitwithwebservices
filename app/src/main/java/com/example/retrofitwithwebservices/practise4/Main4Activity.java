package com.example.retrofitwithwebservices.practise4;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;

import com.example.retrofitwithwebservices.R;
import com.example.retrofitwithwebservices.practise1.Models.Comment;
import com.example.retrofitwithwebservices.practise1.RestApi.ManagerAll;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Main4Activity extends AppCompatActivity {


    List<Comment> comList;
    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main4);

        define();
        request();
    }

    private void define() {


        listView = findViewById(R.id.list_view);
    }


    public void request() {
        comList = new ArrayList<>();
        final Call<List<Comment>> commentList = ManagerAll.getInstance().commentCall();

        commentList.enqueue(new Callback<List<Comment>>() {
            @Override
            public void onResponse(Call<List<Comment>> call, Response<List<Comment>> response) {
                if (response.isSuccessful()) {
                    comList = response.body();

                    AdapterComment adapterComment = new AdapterComment(getApplicationContext(), comList, Main4Activity.this);
                    listView.setAdapter(adapterComment);

                    //Log.i("responseFaruk : ", response.body().toString());
                }


            }

            @Override
            public void onFailure(Call<List<Comment>> call, Throwable t) {
                Log.i("messages :", "hata");
            }
        });


    }
}
