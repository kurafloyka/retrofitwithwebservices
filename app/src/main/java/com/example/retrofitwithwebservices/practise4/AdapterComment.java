package com.example.retrofitwithwebservices.practise4;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.example.retrofitwithwebservices.R;
import com.example.retrofitwithwebservices.practise1.Models.Comment;

import java.util.List;

public class AdapterComment extends BaseAdapter {

    Context context;
    List<Comment> list;
    Activity activity;

    public AdapterComment(Context context, List<Comment> list, Activity activity) {
        this.context = context;
        this.list = list;
        this.activity = activity;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


        convertView = LayoutInflater.from(context).inflate(R.layout.layout4, parent, false);
        LinearLayout linearLayout = convertView.findViewById(R.id.linearList);
        TextView postId = convertView.findViewById(R.id.postId);
        TextView id = convertView.findViewById(R.id.id);
        TextView name = convertView.findViewById(R.id.name);
        TextView email = convertView.findViewById(R.id.email);
        TextView body = convertView.findViewById(R.id.body);

        postId.setText(list.get(position).getPostId().toString());
        id.setText(list.get(position).getId().toString());
        name.setText(list.get(position).getName());
        email.setText(list.get(position).getEmail());
        body.setText(list.get(position).getBody());


        final String idText = id.getText().toString();
        final String postIdText = postId.getText().toString();


        linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(activity, Main5Activity.class);

                intent.putExtra("id", idText);
                intent.putExtra("postId", postIdText);

                activity.startActivity(intent);


            }
        });

        return convertView;
    }
}
