package com.example.retrofitwithwebservices.practise1.RestApi;

public class BaseManager {


    protected RestApi getRestApiClient() {

        RestApiClient restApiClient = new RestApiClient(BaseUrl.postUrl);

        return restApiClient.getRestApi();
    }
}
