package com.example.retrofitwithwebservices.practise1.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.retrofitwithwebservices.R;
import com.example.retrofitwithwebservices.practise1.Models.Post;

import java.util.List;

public class AdapterPost extends BaseAdapter {

    List<Post> list;
    Context context;

    public AdapterPost(List<Post> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = LayoutInflater.from(context).inflate(R.layout.layout, parent, false);

        TextView userId = convertView.findViewById(R.id.userId);
        TextView id = convertView.findViewById(R.id.id);
        TextView title = convertView.findViewById(R.id.title);
        TextView body = convertView.findViewById(R.id.body);

        userId.setText(list.get(position).getUserId().toString());
        id.setText(list.get(position).getId().toString());
        title.setText(list.get(position).getTitle().toString());
        body.setText(list.get(position).getBody().toString());


        return convertView;
    }
}
