package com.example.retrofitwithwebservices.practise1.RestApi;

import com.example.retrofitwithwebservices.practise1.Models.Comment;
import com.example.retrofitwithwebservices.practise1.Models.CommentResult;
import com.example.retrofitwithwebservices.practise1.Models.Photo;
import com.example.retrofitwithwebservices.practise1.Models.Post;
import com.example.retrofitwithwebservices.practise1.Models.Todo;

import java.util.List;

import retrofit2.Call;

public class ManagerAll extends BaseManager {

    private static ManagerAll ourInstance = new ManagerAll();

    public static synchronized ManagerAll getInstance() {
        return ourInstance;
    }

    public Call<List<Post>> postCall() {

        Call<List<Post>> call = getRestApiClient().posts();
        return call;

    }

    public Call<List<Todo>> todoCall() {

        Call<List<Todo>> todo = getRestApiClient().todos();
        return todo;
    }

    public Call<List<Photo>> photoCall() {
        Call<List<Photo>> photo = getRestApiClient().photos();
        return photo;

    }

    public Call<List<Comment>> commentCall() {

        Call<List<Comment>> comment = getRestApiClient().comments();
        return comment;
    }

    public Call<List<CommentResult>> commentResultCall(String postId, String id) {

        Call<List<CommentResult>> commentResult = getRestApiClient().commentResuts(postId, id);
        return commentResult;


    }

}
