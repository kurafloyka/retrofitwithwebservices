package com.example.retrofitwithwebservices.practise1;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.ListView;

import com.example.retrofitwithwebservices.R;
import com.example.retrofitwithwebservices.practise1.Adapter.AdapterPost;
import com.example.retrofitwithwebservices.practise1.Models.Post;
import com.example.retrofitwithwebservices.practise1.RestApi.ManagerAll;
import com.squareup.picasso.Picasso;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MainActivity extends AppCompatActivity {
    List<Post> list;
    ListView listView;
    ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        define();
        request();
        showImage();


    }

    public void showImage() {



        Picasso.with(getApplicationContext()).load("https://news.cgtn.com/news/77416a4e3145544d326b544d354d444d3355444f31457a6333566d54/img/37d598e5a04344da81c76621ba273915/37d598e5a04344da81c76621ba273915.jpg").into(imageView);

    }

    public void define() {

        listView = findViewById(R.id.list_view);
        imageView = findViewById(R.id.imageView);
    }

    public void request() {
        Call<List<Post>> postList = ManagerAll.getInstance().postCall();
        postList.enqueue(new Callback<List<Post>>() {
            @Override
            public void onResponse(Call<List<Post>> call, Response<List<Post>> response) {
                //Log.i("Response : ", response.body().toString());
                if (response.isSuccessful()) {
                    list = response.body();
                    AdapterPost adapter = new AdapterPost(list, getApplicationContext());
                    listView.setAdapter(adapter);
                }

            }

            @Override
            public void onFailure(Call<List<Post>> call, Throwable t) {

            }
        });
    }
}
