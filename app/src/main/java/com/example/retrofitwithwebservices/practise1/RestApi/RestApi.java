package com.example.retrofitwithwebservices.practise1.RestApi;

import com.example.retrofitwithwebservices.practise1.Models.Comment;
import com.example.retrofitwithwebservices.practise1.Models.CommentResult;
import com.example.retrofitwithwebservices.practise1.Models.Photo;
import com.example.retrofitwithwebservices.practise1.Models.Post;
import com.example.retrofitwithwebservices.practise1.Models.Todo;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface RestApi {

    @GET("/posts")
    Call<List<Post>> posts();

    @GET("/todos")
    Call<List<Todo>> todos();

    @GET("/photos")
    Call<List<Photo>> photos();

    @GET("/comments")
    Call<List<Comment>> comments();

    @GET("/comments")
    Call<List<CommentResult>> commentResuts(@Query("postId") String postId, @Query("id") String id);

}
