package com.example.retrofitwithwebservices.practise2;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import com.example.retrofitwithwebservices.R;
import com.example.retrofitwithwebservices.practise1.Models.Todo;

import java.util.List;

public class AdapterTodo extends BaseAdapter {

    Context context;
    List<Todo> list;

    public AdapterTodo(Context context, List<Todo> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        convertView = LayoutInflater.from(context).inflate(R.layout.layout2, parent, false);

        TextView id = convertView.findViewById(R.id.Id);
        TextView userId = convertView.findViewById(R.id.userId);
        TextView title = convertView.findViewById(R.id.title);
        CheckBox completed = convertView.findViewById(R.id.completed);

        id.setText(list.get(position).getId().toString());
        userId.setText(list.get(position).getUserId().toString());
        title.setText(list.get(position).getTitle());

        Boolean value = list.get(position).getCompleted();
        if (value == true) {
            completed.setChecked(true);
        } else {
            completed.setChecked(false);
        }


        return convertView;
    }
}
