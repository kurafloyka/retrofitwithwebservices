package com.example.retrofitwithwebservices.practise2;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ListView;

import com.example.retrofitwithwebservices.R;
import com.example.retrofitwithwebservices.practise1.Models.Todo;
import com.example.retrofitwithwebservices.practise1.RestApi.ManagerAll;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Main2Activity extends AppCompatActivity {


    ListView listViewPost;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        define();
        requestTodos();
    }


    public void requestTodos() {

        Call<List<Todo>> todoList = ManagerAll.getInstance().todoCall();
        todoList.enqueue(new Callback<List<Todo>>() {
            @Override
            public void onResponse(Call<List<Todo>> call, Response<List<Todo>> response) {
                //Log.i("response :", response.body().toString());

                if (response.isSuccessful()) {


                    AdapterTodo adapterTodo = new AdapterTodo(getApplicationContext(), response.body());
                    listViewPost.setAdapter(adapterTodo);


                }
            }

            @Override
            public void onFailure(Call<List<Todo>> call, Throwable t) {

            }
        });
    }

    public void define() {
        listViewPost = findViewById(R.id.listviewPost);
    }
}
