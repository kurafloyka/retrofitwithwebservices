package com.example.retrofitwithwebservices.practise3.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.retrofitwithwebservices.R;
import com.example.retrofitwithwebservices.practise1.Models.Photo;
import com.squareup.picasso.Picasso;

import java.util.List;

public class AdapterPhoto extends BaseAdapter {

    List<Photo> photoList;
    Context context;

    public AdapterPhoto(List<Photo> photoList, Context context) {
        this.photoList = photoList;
        this.context = context;
    }

    @Override
    public int getCount() {
        return photoList.size();
    }

    @Override
    public Object getItem(int position) {
        return photoList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


        convertView = LayoutInflater.from(context).inflate(R.layout.layout3, parent, false);

        TextView albumId = convertView.findViewById(R.id.albumId);
        TextView id = convertView.findViewById(R.id.id);
        TextView title = convertView.findViewById(R.id.title);
        TextView url = convertView.findViewById(R.id.url);
        ImageView imageView = convertView.findViewById(R.id.thumbnailUrl);

        albumId.setText(photoList.get(position).getId().toString());
        id.setText(photoList.get(position).getId().toString());
        title.setText(photoList.get(position).getTitle().toString());
        url.setText(photoList.get(position).getUrl().toString());

        Picasso.with(context).load(photoList.get(position).getThumbnailUrl()).into(imageView);


        return convertView;
    }
}
