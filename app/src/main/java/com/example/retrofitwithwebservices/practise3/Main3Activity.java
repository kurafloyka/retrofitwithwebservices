package com.example.retrofitwithwebservices.practise3;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;

import com.example.retrofitwithwebservices.R;
import com.example.retrofitwithwebservices.practise1.Models.Photo;
import com.example.retrofitwithwebservices.practise1.RestApi.ManagerAll;
import com.example.retrofitwithwebservices.practise3.Adapter.AdapterPhoto;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Main3Activity extends AppCompatActivity {
    ListView listView;
    List<Photo> photos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);
        define();
        request();
    }

    private void request() {
        photos = new ArrayList<>();
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("All Photos");
        progressDialog.setMessage("All Photos are loading....");
        progressDialog.setCancelable(false);
        progressDialog.show();

        Call<List<Photo>> photoList = ManagerAll.getInstance().photoCall();

        photoList.enqueue(new Callback<List<Photo>>() {

            @Override
            public void onResponse(Call<List<Photo>> call, Response<List<Photo>> response) {
                photos = response.body();
                //Log.i("Response :", response.body().toString());
                if (response.isSuccessful()) {

                    AdapterPhoto adapterPhoto = new AdapterPhoto(photos, getApplicationContext());
                    listView.setAdapter(adapterPhoto);
                }
                progressDialog.cancel();
            }

            @Override
            public void onFailure(Call<List<Photo>> call, Throwable t) {

            }
        });
    }

    public void define() {
        listView = findViewById(R.id.list_view_photos);
    }
}
